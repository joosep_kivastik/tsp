import pygame, sys, time, copy, random
from pygame.locals import *

WINDOWWIDTH = 600
WINDOWHEIGHT = 600
mousex = 0
mousey = 0

cafes = [[415, 393], [1044, 601]]
cities = []
cities1 = [cafes[0]]
cities2 = [cafes[1]]
roadTest = []
LENGTH = 1000000000
LENGTH1 = 1000000000
LENGTH2 = 1000000000
ROADS = []
ROADS1 = []
ROADS2 = []
LENGTHTEST = 0
LENGTHTEST1 = 0
LENGTHTEST2 = 0


def roadLength(px1,py1,px2,py2):
    return ((px1-px2)**2+(py1-py2)**2)**0.5

def totalLength(roads):
    length = 0
    for road in roads:
        length += roadLength(road[0], road[1], road[2], road[3])
    return length

#Greedy algorithm for TSP. The salesman always goes to the next nearest city.
#Tries out every city as a starting point and finds the shortest greedy route.
def greedy(cities, index):
    global roadTest
    citiez = copy.copy(cities)
    city1 = citiez[index]
    citiez.remove(city1)
    
    if citiez == []:
        return
    
    closestLength = 1000000
    closestCity = city1
    
    for city in citiez:
        a = roadLength(city1[0],city1[1],city[0],city[1])
        if a < closestLength:
            closestLength = a
            closestCity = city
      
    roadTest.append([city1[0], city1[1], closestCity[0], closestCity[1]])
    pygame.draw.line(DISPLAY, [0,0,0], [city1[0],city1[1]], [closestCity[0],closestCity[1]], 1)
    pygame.display.update()
    
    greedy(citiez, citiez.index(closestCity))
    return

#Randomly generates a city inside the display area
def generateCity():
    colors = [[200,0,0], [0,200,0]]
    color = random.choice(colors)
    city_x = random.randrange(30,WINDOWWIDTH-30)
    city_y = random.randrange(30,WINDOWHEIGHT-30)
    pygame.draw.circle(DISPLAY, [0,0,0] ,[city_x, city_y], 8)
    cities.append([city_x,city_y, color])

'''def drawCafes(cafes):
    pygame.draw.circle(DISPLAY, [255,0,0], cafes[0], 20)
    pygame.draw.circle(DISPLAY, [0,255,0], cafes[1], 20)'''
    
#Checks if the points are counter clocwkise
#source http://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
def ccw(A, B, C):
    return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])

#Checks if two lines intersect
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

#Checks if the the whole road is continuous from start to end
def continuousRoadCheck(roads, index):
    for road in roads:
        print "All the roads: %d, %d, %d, %d" % (road[0], road[1], road[2], road[3])
    numOfRoads = 0
    roadz = copy.copy(roads)
    road = roadz[index]
    roadz.remove(road)
    print "Cont check road1: %d, %d, %d, %d" % (road[0], road[1], road[2], road[3])
    if roadz == []:
        return 1
    for road2 in roadz:
        print "Cont check road2: %d, %d, %d, %d" % (road2[0], road2[1], road2[2], road2[3])
        if ((road[0] == road2[0]) and (road[1] == road2[1])) or ((road[0] == road2[2]) and (road[1] == road2[3])) \
           or ((road[2] == road2[0]) and (road[3] == road2[1])) or ((road[2] == road2[2]) and (road[3] == road2[3])):
            numOfRoads += 1
            numOfRoads += continuousRoadCheck(roadz, roadz.index(road2))
            break
    print "Roads: %d" % (numOfRoads)
    return numOfRoads

                
#Two-opt algorithm. It takes in an already drawn road and searches for intersections,
#then it sees if swapping makes the route shorter also checks if the route stays continuous.
def twoOpt(index):
    global ROADS, numOfRoads
    roadz = copy.copy(ROADS)
    road1 = roadz[index]
    
    if roadz.index(road1) != (len(roadz)-1):
        del roadz[roadz.index(road1)+1]
        
    if roadz.index(road1) != 0:
        del roadz[roadz.index(road1)-1]
        
    roadz.remove(road1)
    
   
    for road2 in roadz:
        newRoad1 = []
        newRoad2 = []

        if intersect([road1[0], road1[1]], [road1[2],road1[3]], [road2[0], road2[1]], [road2[2], road2[3]]) == True:
            length = roadLength(road1[0], road1[1], road1[2], road1[3]) + roadLength(road2[0], road2[1], road2[2], road2[3])
            length2 = roadLength(road1[0], road1[1], road2[0], road2[1]) + roadLength(road1[2], road1[3], road2[2], road2[3])
            length3 = roadLength(road1[0], road1[1], road2[2], road2[3]) + roadLength(road1[2], road1[3], road2[0], road2[1])

            if length2 < length:
                newRoad1 = [road1[0], road1[1], road2[0], road2[1]]
                newRoad2 = [road1[2], road1[3], road2[2], road2[3]]
                roadTest = copy.copy(ROADS)
                roadTest[index] = newRoad1
                roadTest[roadTest.index(road2)] = newRoad2

                if continuousRoadCheck(roadTest,0) == len(ROADS):
                    ROADS[index] = newRoad1
                    ROADS[ROADS.index(road2)] = newRoad2

                    if length3 < length2:
                        roadTest = copy.copy(ROADS)
                        roadTest[roadTest.index(newRoad1)] = [road1[0], road1[1], road2[2], road2[3]]
                        roadTest[roadTest.index(newRoad2)] = [road1[2], road1[3], road2[0], road2[1]]

                        if continuousRoadCheck(roadTest,0) == len(ROADS):
                            ROADS[ROADS.index(newRoad1)] = [road1[0], road1[1], road2[2], road2[3]]
                            ROADS[ROADS.index(newRoad2)] = [road1[2], road1[3], road2[0], road2[1]]


            elif length3 < length:
                newRoad1 = [road1[0], road1[1], road2[2], road2[3]]
                newRoad2 = [road1[2], road1[3], road2[0], road2[1]]

                roadTest = copy.copy(ROADS)
                roadTest[index] = newRoad1
                roadTest[roadTest.index(road2)] = newRoad2

                if continuousRoadCheck(roadTest,0) == len(ROADS):
                    ROADS[index] = newRoad1
                    ROADS[ROADS.index(road2)] = newRoad2

                    if length2 < length3:
                        roadTest = copy.copy(ROADS)
                        roadTest[roadTest.index(newRoad1)] = [road1[0], road1[1], road2[0], road2[1]]
                        roadTest[roadTest.index(newRoad2)] = [road1[2], road1[3], road2[2], road2[3]]

                        if continuousRoadCheck(roadTest,0) == len(ROADS):
                            ROADS[ROADS.index(newRoad1)] = [road1[0], road1[1], road2[0], road2[1]]
                            ROADS[ROADS.index(newRoad2)] = [road1[2], road1[3], road2[2], road2[3]]
          
    return
            

def buildCitiesClean(cities):
    DISPLAY.fill([255,255,255])
    for city in cities:
        pygame.draw.circle(DISPLAY, [0,0,0] , [city[0],city[1]], 5)
        #textSurface = myfont.render(str(cities.index(city)), False, (0,0,0))
        #DISPLAY.blit(textSurface, (city[0]-30, city[1]-30))

def buildRoads(roads, color):
    for road in roads:
        pygame.draw.line(DISPLAY, color, [road[0],road[1]], [road[2],road[3]], 1)

            


pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont("Comic Sans MS", 30)

DISPLAY = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), 0 , 32)
pygame.display.set_caption('Kompfys TSP REV A')

while True:
    
    buildCitiesClean(cities)
    #drawCafes(cafes)
    buildRoads(ROADS, [0, 0, 0])

    
        
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()

        elif event.type == MOUSEMOTION:
            mousex, mousey = event.pos

        elif event.type == MOUSEBUTTONUP:
            mousex, mousey = event.pos
            pygame.draw.circle(DISPLAY, [125,125,125] ,[mousex, mousey], 10)
            cities.append([mousex,mousey,[],0])

        elif event.type == KEYUP and event.key == K_b:
            generateCity()

        elif event.type == KEYUP and event.key == K_r:
            f = open('data.txt', 'w')
            for sets in range(0,10):
                generateCity()
                startTime = time.time()
                for experiment in range(0,80):    
                    generateCity()

                    for i in range(0, len(cities)-1):
                        roadTest = []
                        testLength = 0
                        greedy(cities, i)
                        testLength = totalLength(roadTest)
                        print "Current greedy length is %.1f " % testLength
                        
                        if testLength < LENGTH:
                            LENGTH = testLength
                            ROADS = copy.copy(roadTest)

                        buildCitiesClean(cities)
                        pygame.display.update()
                        print "The shortest greedy length was %.1f " % LENGTH
                        Greedy = LENGTH
                        greedyTime = time.time()

                    oldLength = totalLength(ROADS)
                    while True:
                        for i in range(0, len(ROADS)-1):
                            twoOpt(i)
                            buildCitiesClean(cities)
                            buildRoads(ROADS, [0, 0, 0])
                            pygame.display.update()
                        if (totalLength(ROADS) - oldLength) == 0:
                            break
                        else:
                            oldLength = totalLength(ROADS)                
                            print "Current length is %.1f " % totalLength(ROADS)
                               
                    elapsedTime  = time.time() - startTime  
                    print "Time the algorithm ran: %.2f " % elapsedTime
                    string = str(len(cities)) + ", " + str("%.2f" % greedyTime) + ", " +  str("%.2f" % elapsedTime)+ ", " + str(Greedy) + ", " + str(totalLength(ROADS)) + "\n"
                    
                    f.write(string)
                    roadTest = []
                    LENGTH = 1000000000
                    ROADS = []
                    LENGTHTEST = 0
                    buildCitiesClean(cities)
                    pygame.display.update()
                cities = []
                buildCitiesClean(cities)
                pygame.display.update()
            f.close()

        elif event.type == KEYUP and event.key == K_f:
            for i in range(0, len(cities)-1):
                roadTest = []
                testLength = 0
                greedy(cities, i)
                #roadTest.append([roadTest[-1][2],roadTest[-1][3],roadTest[0][0],roadTest[0][1]])
                testLength = totalLength(roadTest)
                print "Current greedy length is %.1f " % testLength
                        
                if testLength < LENGTH:
                    LENGTH = testLength
                    ROADS = copy.copy(roadTest)
                    buildCitiesClean(cities)
                    pygame.display.update()
                    print "The shortest greedy length was %.1f " % LENGTH

        elif event.type == KEYUP and event.key == K_u:
                oldLength = totalLength(ROADS)
                while True:
                    for i in range(0, len(ROADS)-1):
                        twoOpt(i)
                        buildCitiesClean(cities)
                        buildRoads(ROADS, [0,0,0])
                        pygame.display.update()
                    if (totalLength(ROADS) - oldLength) == 0:
                        break
                    else:
                        oldLength = totalLength(ROADS)                
                        print "Current length is %.1f " % totalLength(ROADS)
                

        elif event.type == KEYUP and event.key == K_g:
            for j in range (0, 1):
                cities1.append(cities[j])
                roadTest = []
                greedy(cities1, 0)

                testLength = totalLength(roadTest)
                if testLength < LENGTH1:
                    LENGTH1 = testLength
                    ROADS1 = copy.copy(roadTest)

                roadTest = []
                for m in range(j+1, len(cities)-j):
                    cities2.append(cities[m])
                    
                greedy(cities2, 0)
                testLength = totalLength(roadTest)
                if testLength < LENGTH2:
                    LENGTH2 = testLength
                    ROADS2 = copy.copy(roadTest)

                buildCitiesClean(cities)
                pygame.display.update()
                #drawCafes(cafes)
                print "The shortest greedy length was %.1f " % (LENGTH1 + LENGTH2)
            

        elif event.type == KEYUP and event.key == K_c:

            roadTest = []
            LENGTH = 1000000000
            ROADS = []
            LENGTHTEST = 0
            buildCitiesClean(cities)
            pygame.display.update()

        pygame.display.update()
    
